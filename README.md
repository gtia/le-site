# Dépot du code du site "Groupe de travail sur l'I.A. d'Orléans-Tours"

- [Lien vers le site](https://gtia.forge.apps.education.fr/le-site)
- [Lien vers le WIKI](https://forge.aeif.fr/LEMENTEC/gtia/-/wikis/Cr%C3%A9ation-d'un-article)

## Ressources utiles

- [Tutoriel interactif `Markdown`](https://www.markdowntutorial.com/fr/)
- [Documentation `Hugo`](https://gohugo.io/documentation/)