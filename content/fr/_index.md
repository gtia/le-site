---
title: "Site du groupe de travail sur l'Intelligence Artificielle"

description: "Académie d'Orléans-Tours"
cascade:
  featured_image: '/images/Baniere_GTIA.png'
---

Ce site vous permet de trouver toutes les ressources produites par le groupe I.A. de l'académie d'Orléans-Tours.  
Vous trouverez **des articles**, **des ressources**, **des activités**...  

Vous pouvez filtrer les activités en fonction de différents critères à partir du menu `Filtres` dans le bandeau supérieur.

Bonne lecture !