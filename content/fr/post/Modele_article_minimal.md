---
date: 2023-08-01
title: "Titre de l'article"
description: "Sous titre"
# Le résumé est le petit texte qui apparaît à coté du titre avant de cliquer sur l'article
summary: "Résumé "
# Les images sont localisées dans le dossier /static/img/ 
# Pas besoin de faire figurer /static dans l'url ci-dessous
featured_image: "/img/Baniere_GTIA.png"
# Les tags doivent tous être remplis
# Attention : pas d'accents dans les tags ! 
tags: ["Sante"]
niveaux: ["College"]
duree: "1 seance"
enseignements: ["SVT","Maths"]
# Basculer sur False pour publier
draft: True
---

# Titre de niveau 1

Bla bla bla

<!-- Lien hypertexte -->
[Texte](lien vers la ressourse)

<!-- Lien vers image  -->
<!-- Image à placer dans le dossier /static/img -->
<!-- L'adresse de l'image devra être de la forme ci-dessous -->
<!-- /gtia/ remplace le dossier /static/ -->
![Image tulipe](/gtia/img/Repres-tulipe1.jpg)