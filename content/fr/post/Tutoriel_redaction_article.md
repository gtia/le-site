---
date: 2023-08-01
description: "Tutoriel pour article"
title: "Tutoriel pour article"
featured_image: "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/langfr-1920px-Markdown-mark.svg.png"
tags: ["Redaction"]
niveaux: ["Prof"]
enseignements: ["Informatique"]
duree: "30 minutes"
draft: False
---

# Des essais de liaison `NextCloud` & `Forge`

## Une image depuis `NextCloud`
![Image test](https://nuage03.apps.education.fr/index.php/apps/files_sharing/publicpreview/MBSPX4s99aTonpB?file=/Logo/logo-gtia-turquoise-trans.png&fileId=115042650&x=1920&y=1080&a=true)

## Un lien vers une ressource hébergée sur le `NextCloud`
[Lien vers la fiche pédagogique](https://nuage03.apps.education.fr/index.php/s/MBSPX4s99aTonpB/download/115042565)

> Il manque un moyen pour donner simplement un lien vers `NextCloud`, il faudrait partager les fichiers je pense...

#  Mise en forme en Markdown 
Si vous lisez l'anglais, vous pouvez aller directement sur [markdownguide](https://www.markdownguide.org/basic-syntax/) qui explique (presque) tout en détail.  
Sinon, en français sur [framasoft](https://docs.framasoft.org/fr/grav/markdown.html).  
En voici un résumé.

## Des titres et des sous-titres pour structurer

`# Titre 1`  
`## Titre 2`  
`### Titre 3`  
`#### Titre 4`  

## Composer et mettre en forme du texte 

Des balises pour du texte `*italique*`,  `**gras**`, et même du \`code()\`.

`- Une liste`  
`- avec des puces`  

`1. Une liste`  
`2. avec des numéros`  

Saut de ligne : Ajouter 2 espaces en fin de ligne ou sauter une ligne entre les 2 paragraphes

## Des images et des liens

`![image alt](https://en.wikipedia.org/wiki/Markdown#/media/File:Markdown-mark.svg) `   
![image alt](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1920px-Markdown-mark.svg.png)  

- Les adresses sont traduites en lien cliquable : http://w3.org
- On peut nommer les liens : [Google](http://google.com)
- On peut référencer les liens : [wikipedia][WP]

[WP]: https://fr.wikipedia.org/ "En français"

```markdown
- Les adresses sont traduites en lien cliquable : http://w3.org
- On peut nommer les liens : [Google](http://google.com)
- On peut référencer les liens : [wikipedia][WP]

[WP]: https://fr.wikipedia.org/ "En français"
```

## Tableaux

| Header   | L      | R      | C
| ---      | :---   | ---:   | :---:
| Cellule  | LLLLL  | RRRRR  | CCCCC
| Cell     | L      | R      | C

```markdown
| Header   | L      | R      | C
| ---      | :---   | ---:   | :---:
| Cellule  | LLLLL  | RRRRR  | CCCCC
| Cell     | L      | R      | C
```



