---
date: 2023-08-01
title: "Imagerie médicale et I.A."
description: "Médecine : l’Intelligence artificielle au service de la prévention du cancer du sein."
summary: "Une activité pédagogique utilisant la programmation en Python permet de prendre conscience de ce qu’est l’apprentissage supervisé. Dans cette activité, nullement besoin de savoir programmer, il s’agit de comprendre comment fonctionne l’apprentissage supervisé à partir d’un exemple concret dans le cadre de l’analyse de mammographies. "
featured_image: "/img/mammographie.jpg"
tags: ["Sante"]
niveaux: ["College"]
duree: "1 seance"
enseignements: ["SVT","Maths"]
draft: False
---

# La mammographie : une technique radiologique permettant d’obtenir des images de l’intérieur du sein.

L’utilisation des rayons X pour obtenir des images de l’intérieur du corps date de 1895 ; Wilhelm Röntgen réalise la première radiographie de l’Histoire, celle de la main de son épouse Berta. Dès le début de l’année 1896, les premiers service d’imagerie médicale ouvrent. Röntgen reçu le prix Nobel de physique en 1901.

Cette technique radiographique est aujourd’hui utilisée afin d’obtenir des images de l’intérieur des seins : c’est la mammographie. Elle permet de détecter des anomalies dans les seins.
Cet examen est réalisé par un radiologue, la radio étant interprétée ensuite par un médecin.

![Image](/le-site/img/main_Berta.png)

La classification du résultat utilise le système BIRADS de l’American College of Radiology (ACR) afin d’obtenir 6 niveaux de résultat :
- ACR 0 : classification d’attente, quand des investigations complémentaires sont nécessaires
- ACR 1 : mammographie normale
- ACR 2 : il existe des anomalies bénignes (c’est-à-dire sans gravité) qui ne nécessitent ni surveillance ni examen complémentaire 
- ACR 3 : il existe une anomalie probablement bénigne pour laquelle une surveillance à court terme (3 ou 6 mois) est conseillée 
- ACR 4 : il existe une anomalie indéterminée ou suspecte
- ACR 5 : il existe une anomalie évocatrice d’un cancer
Intervention de l’Intelligence Artificielle dans l’interprétation des mammographies.

![Image](/gtia/img/mammographie1.png)

Une des technologies de l’Intelligence Artificielle est le Deep Learning (Apprentissage profond). Il s’agit d'algorithmes capables de mimer les actions du cerveau humain grâce à des réseaux de neurones artificiels. Ceux ci sont composés de dizaines ou de centaines de « couches » de neurones, chacune recevant et interprétant les informations de la couche précédente. Un point important de cette technologie est que ces modèles de Deep learning ont tendance à bien fonctionner avec une grande quantité de données (Big datas).

![Image](/gtia/img/deep_learning.png)

Un algorithme développé par des chercheurs du Massachusets Institute of Technology (MIT) est aujourd’hui capable de prédire l’apparition d’une tumeur du sein avec 4 ans d’avance sur l’analyse d’une mammographie par un humain, les indices étant alors indétectables pour un médecin. Un résultat mettant en évidence l’apport de l’Intelligence Artificielle dans le dépistage des cancers. En effet, la survie à 5 ans pour un cancer métastasé est de seulement 26 %, contre 99 % si la tumeur est détectée précocement.
L'algorithme a montré qu'il était capable d'identifier une femme à haut risque de cancer du sein (à gauche) quatre ans avant qu'il n'apparaisse à l'imagerie (à droite). Photo Radiology / A Yala et al. 

![Image](/gtia/img/mammographie2.png)

# L’apprentissage supervisé
Nous nous intéresserons davantage au mode d’apprentissage de notre IA qu’à son mode de fonctionnement en termes de réseaux neuronaux.
Dans le cadre d’un apprentissage supervisé d’une machine apprenante, on utilise une grande quantité de données déjà analysées qui va servir de base à la machine pour prédire ensuite des prédictions sur des données non analysées.

Une activité pédagogique utilisant la programmation en Python permet de prendre conscience de ce qu’est l’apprentissage supervisé. Dans cette activité, nullement besoin de savoir programmer, il s’agit de comprendre comment fonctionne l’apprentissage supervisé à partir d’un exemple concret dans le cadre de l’analyse de mammographies.
Cet exercice est basé sur un jeu de données comprenant des mesures sur les mammographies de 569 patientes sur la base d’une étude menée en collaboration avec l’université du Wisconsin.

Code activité sur CAPYTALE : `60b1-1149704`  
[Lien vers l'activité CAPYTALE](https://capytale2.ac-paris.fr/web/code/60b1-1149704)