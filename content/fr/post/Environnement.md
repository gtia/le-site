---
date: 2023-08-01
description: "Environnement et I.A."
title: "Environnement et I.A."
tags: ["Environnement"]
niveaux: ["Lycee"]
enseignements: ["Physique","Maths"]
duree: "2 seances"
draft: False
---

# Environnement et I.A.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Verba tu fingas et ea dicas, quae non sentias? Quam nemo umquam voluptatem appellavit, appellat; Sed tamen intellego quid velit. Nam Pyrrho, Aristo, Erillus iam diu abiecti. Duo Reges: constructio interrete. Minime vero istorum quidem, inquit. 

Iam id ipsum absurdum, maximum malum neglegi. Nunc vides, quid faciat. Quae ista amicitia est? Neutrum vero, inquit ille. Scaevolam M. Stoici scilicet. At iam decimum annum in spelunca iacet. 

Nulla erit controversia. Tum Torquatus: Prorsus, inquit, assentior; Sine ea igitur iucunde negat posse se vivere? Certe, nisi voluptatem tanti aestimaretis. Sed haec omittamus; 

Sed potestne rerum maior esse dissensio? Nunc haec primum fortasse audientis servire debemus. 

Bonum valitudo: miser morbus. Bork Nihil opus est exemplis hoc facere longius. Ut pulsi recurrant? Quid ad utilitatem tantae pecuniae? 