---
date: 2023-08-01
title: "IA et représentation"
description: "Nos représentations de l'IA et les représentations avec l'IA."
# Le résumé est le petit texte qui apparaît à coté du titre avant de cliquer sur l'article
summary: "Le groupe IA et Représentaton propose des activités pour voir comment on peut utiliser l'IA pour représenter des images, de la musique, des émotions et s'interroge d'autre part sur notre représentation de l'IA pour mieux expliquer l'IA aux élèves."
# Les images sont localisées dans le dossier /static/img/ 
# Pas besoin de faire figurer /static dans l'url ci-dessous
featured_image: "/img/Baniere_GTIA.png"
# Les tags doivent tous être remplis
tags: ["Representation"]
niveaux: ["College", "Lycee"]
duree: "5 seances"
enseignements: ["Art plastique","Musique", "Technologie"]
# Basculer sur False pour publier
draft: False
---

# IA et représentation


L'intelligence artificielle  est devenue une technologie de plus en plus omniprésente dans notre vie quotidienne. Dans ce document, nous allons aborder la thématique de la représentation en IA sous deux axes complémentaires : représenter l’IA pour la comprendre puis l’IA au service de la représentation.

##  Représenter l’IA pour la comprendre

### Introduction
L’IA repose sur la capacité combinatoire de données diversement acquises et analysées. Les solutions mathématiques et statistiques sont privilégiées, renforçant les biais de l'indexation première. Pourtant, il existe une autre manière d'appréhender le réel. Les artistes jouent avec notre connaissance du monde pour montrer sa part inconnue, invisible, bien que présente sous nos yeux. Il ne s’agit pas d’éclairer ce que nous savons déjà, mais de dévoiler ce que nous ne savons pas encore.

Plus que d’une démarche de recoupement, l’IA pourrait gagner à s’inspirer d’une heuristique convoquant l’écart plus que la similitude dans les rapprochements qu’elle opère. Convoquer autant Alan Turing qu’Aby Warburg ouvre de nouvelles perspectives pour une IA plus proche finalement de la pensée humaine, dont le pouvoir métaphorique est cruellement manquant aux ordinateurs.

### Apprentissage supervisé : exercice.
On dispose de plusieurs photos de tulipes différentes. Nous connaissons pour partie d’entre elles la famille à laquelle elles appartiennent (tulipes dentelées, Darwin, Triomphe, etc.). Comment serons-nous capable de reconnaître de nouvelles photos et la variété des nouvelles venues ? Les stratégies adoptées sont celles de l’Intelligence artificielle avec apprentissage supervisé.

![Image tulipe](/gtia/img/Repres-tulipe1.jpg)

### Du mot à l’image : exercice.
En groupe les élèves étudient une image - la décrivent, écrivent un algorithme (description écrite et précise de l'œuvre) pour le représenter puis d'autres élèves suivent le code pour le représenter.

![Image avec trois oeuvres](/gtia/img/Repres-Harring-Warhol-Vinci.png)

Objectifs :
* montrer l'importance des données initiales (nombre, précision...)
* montrer que le résultat dépend de l'algorithme écrit (tous les élèves ont-ils réalisé une description similaire de l'objet ?
* montrer l'influence de l'algorithme sur la représentation.

L'algorithme peut alors être modifié, amélioré pour se rapprocher le plus du modèle d'origine ou des besoins des utilisateurs.

## L’IA au service de la représentation
###	Comment l’IA peut représenter les postures et les émotions humaines ?


Catherine Pelachaud (Directrice de recherche au CNRS et à Télécom-ParisTech) fabrique des « agents conversationnels » appelés aussi « chatbot » capables de discuter avec des êtres humains.
Pour fabriquer ce type de machine, l’émotion apparait comme un élément essentiel, c’est pourquoi son champ de recherche concerne les signes extérieurs d’émotion transmis par le chatbot 
« La machine ne ressent pas, mais elle peut transmettre, souligne la chercheuse. Le ressenti est du domaine de l’homme, et ça doit le rester ! Une machine est là pour palier des besoins. Pour cela, la simulation peut suffire ». 
L’informatique affective des chatbot ne se contente pas de mimer les émotions de base (tristesse, joie, peur, colère, surprise, dégoût). Ils doivent aussi être en mesure de détecter celles des humains, et de s’y adapter en temps réel.
Cependant le Japonais Masahiro Mori met en garde que si le réalisme humain d’une machine est trop important, « on tombe dans la vallée de l’étrange ».
Les représentations très réalistes, mais toujours imparfaites, de l’homme, nous paraissent alors dérangeantes, voire monstrueuses. Nous trouvons plus sympathiques et ressentons de l’empathie pour des représentations de l’humain bien plus schématiques. 

#### Activité 1  
Les élèves de 4e du collège collège Saint Exupéry de Bourges ont travaillé sur la problématique :  « comment programmer la carte Microbit pour simuler les émotions humaines ? » 
Ils ont aussi imaginé le design de leur objet 

Utilisation d’un algorithme, capteurs, actionneur (signal visuel et sonore)
[Télécharger la fiche de l'activité 1](https://nuage03.apps.education.fr/index.php/s/rar8CKLCjRMG5xn)

>[static/img/Repres-Activite1] à suivre...
#### Activité 2  
Les élèves ont créé une base de données pour apprendre à leur Tamagotghi a reconnaitre leurs visages et ont écrit un programme capable d’afficher sur la carte Microbit l’initiale du prénom de la personne reconnue grâce à l’intelligence artificielle.
[Dossier avec les fiches de l'activité 2](https://nuage03.apps.education.fr/index.php/s/SGg6tffr9Rf7yRo)